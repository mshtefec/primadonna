<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * UserType form.
 * @author Max Shtefec <max.shtefec@gmail.com>
 */
class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('apellido', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('nacionalidad', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('ocupacion', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('detalle', TextareaType::class, array(
                'label' => 'Detalle',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Telefono',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('celular', TextType::class, array(
                'label' => 'Celular',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('enabled', null, array(
                'label' => 'Activo',
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('username', null, array(
                'label' => 'Nombre de Usuario',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('email', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('password', null, array(
                'label' => 'Contraseña',
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('imageFile', VichImageType::class, array(
                'required'      => false,
                'allow_delete'  => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'BackBundle_user';
    }

}
