<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\UserBundle\Util\LegacyFormHelper;

/**
 * UserType form.
 * @author Max Shtefec <max.shtefec@gmail.com>
 */
class PerfilType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('apellido', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('nacionalidad', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('ocupacion', TextType::class, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('detalle', TextareaType::class, array(
                'label' => 'Detalle',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Telefono',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('celular', TextType::class, array(
                'label' => 'Celular',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('username', null, array(
                'label' => 'Nombre de Usuario',
                'label_attr' => array(
                    'class' => 'col-lg-12 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('email', null, array(
                'label_attr' => array(
                    'class' => 'col-lg-2 control-label',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array(
                        'label' => 'form.password',
                        'attr' => array(
                            'class' => 'form-control',
                        )
                    ),
                'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'attr' => array(
                            'class' => 'form-control',
                        )
                    ),
                'invalid_message' => 'fos_user.password.mismatch',
                'required' => false,
            ))
            ->add('imageFile', VichImageType::class, array(
                'label'         => 'Imagen de Perfil',
                'required'      => false,
                'allow_delete'  => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'BackBundle_perfil';
    }

}
