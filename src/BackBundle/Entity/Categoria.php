<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prenda
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $nombre;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="BackBundle\Entity\Categoria", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $padre;

    /**
     * @var category
     *
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Categoria", mappedBy="padre", cascade={"remove"}, orphanRemoval=true)
     */
    private $children;

    public function __toString() {
        return $this->nombre;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set padre
     *
     * @param \BackBundle\Entity\Categoria $padre
     *
     * @return Categoria
     */
    public function setPadre(\BackBundle\Entity\Categoria $padre = null)
    {
        $this->padre = $padre;
    
        return $this;
    }

    /**
     * Get padre
     *
     * @return \BackBundle\Entity\Categoria
     */
    public function getPadre()
    {
        return $this->padre;
    }

    /**
     * Add child
     *
     * @param \BackBundle\Entity\Categoria $child
     *
     * @return Categoria
     */
    public function addChild(\BackBundle\Entity\Categoria $child)
    {
        $this->children[] = $child;
    
        return $this;
    }

    /**
     * Remove child
     *
     * @param \BackBundle\Entity\Categoria $child
     */
    public function removeChild(\BackBundle\Entity\Categoria $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
